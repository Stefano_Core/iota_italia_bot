import telepot
from telepot.loop import MessageLoop
import time
import json

"Initialize bot"
TOKEN = 'MY_BOT_TOKEN'
myBot = telepot.Bot(TOKEN)

while True:
    try:
        with open('/home/Iota_nodes_list_bot/nodes_list.txt') as nodesListFile:
            nodeList = json.load(nodesListFile)
        print('Opening file...')
        print("Initialization completed!")
        break
    except:
        print("'nodes_list.txt' file not found...\n")
        print("Please create the file and place it in script folder...\n")
        input("After file creation, press enter to retry...\n\n");


"Testing the bot"
print(myBot.getMe())

"Define the message listener function"
def messageListener(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    if content_type == 'text':
        if (msg['text'] == '/node_list') or (msg['text'] == '/node_list@IotaItaliaBot'):
            user_id = msg['chat']['id']
            messageToSend = 'Lista dei nodi gestiti da membri di IOTA ITALIA: ' + '\n'
            for node in nodeList['nodeList']:
                messageToSend += (node + '\n')
            messageToSend += '\n' + 'Per info: @IotaFullNodeITA'
            myBot.sendMessage(user_id, messageToSend)


"Activate listineing loop"
MessageLoop(myBot, messageListener).run_as_thread()
print('Listening ...')


"Keep the program running."
while 1:
    time.sleep(3)

